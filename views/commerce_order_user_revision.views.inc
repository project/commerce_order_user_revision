<?php

/**
 * @file
 * Provides Views integration for Commerce Order: User Revision module.
 */

/**
 * Implements hook_views_data_alter().
 */
function commerce_order_user_revision_views_data_alter(&$data) {
  if (isset($data['commerce_order'])) {
    $data['commerce_order']['user_vid'] = array(
      'title' => t('Owner Revision ID'),
      'help' => t("The unique ID of the order owner's revision."),
      'argument' => array(
        'handler' => 'views_handler_argument_user_vid',
        'click sortable' => TRUE,
        'numeric' => TRUE,
      ),
      'field' => array(
        'click sortable' => TRUE,
      ),
      'filter' => array(
        'handler' => 'views_handler_filter_numeric',
      ),
      'sort' => array(
        'handler' => 'views_handler_sort',
      ),
      'relationship' => array(
        'handler' => 'views_handler_relationship',
        'base' => 'user_revision',
        'base field' => 'vid',
        'title' => t('Owner revision'),
        'label' => t("Relate this order to its owner's user revision."),
      ),
    );
    $data['commerce_order']['table']['join'] = array(
      'user_revision' => array(
        'left_field' => 'vid',
        'field' => 'user_vid',
        'type' => 'INNER',
      ),
    );
  }
}
